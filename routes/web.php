<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use \Illuminate\Support\Facades\Route;

Route::group(['prefix' => 'videos', 'middleware' => 'load_suggest_post'], function () {
    Route::get('', 'VideosController@indexView')->name('videos.index');
    Route::get('index.json', 'VideosController@index');
    Route::get('more-content', 'VideosController@loadNewVideoComponentView')->name('videos.more_content');
    Route::get('old.json', 'VideosController@old');
    Route::get('old', 'VideosController@oldView')->name('videos.old');
    Route::get('hot.json', 'VideosController@hot');
    Route::get('hot', 'VideosController@hotView')->name('videos.hot');
    Route::get('{handle_url}.json', 'VideosController@singleVideo');
    Route::get('{handle_url}', 'VideosController@singleVideoView')->name('videos.single');
});

//Route::group(['prefix' => 'api'], function () {
//
//    Route::group(['prefix' => 'videos'], function () {
//        Route::get('/', 'VideosController@index');
//        Route::get('/old', 'VideosController@old');
//        Route::get('/hot', 'VideosController@hot');
//        Route::get('/{handle_url}', 'VideosController@singleVideo');
//    });
//
//    Route::group(['prefix' => ''], function () {
//        Route::get('', 'IndexController@index');
//        Route::get('/old', 'IndexController@old');
//        Route::get('/hot', 'IndexController@hot');
//        Route::get('/{handle_url}', 'IndexController@singleImage');
//    });
//
//});

Route::group(['prefix' => '', 'middleware' => 'load_suggest_post'], function() {
    Route::get('', 'IndexController@indexView')->name('index');
    Route::get('index.json', 'IndexController@index');
    Route::get('more-content', 'IndexController@loadNewImageComponentView')->name('index.more_content');
    Route::get('old.json', 'IndexController@old');
    Route::get('old', 'IndexController@oldView')->name('index.old');
    Route::get('hot.json', 'IndexController@hot');
    Route::get('hot', 'IndexController@hotView')->name('index.hot');
    Route::get('{handle_url}.json', 'IndexController@singleImage');
    Route::get('{handle_url}', 'IndexController@singleImageView')->name('index.single');
});

