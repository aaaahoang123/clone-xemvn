<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    public function posts() {
        return $this->hasMany('App\Post');
    }
}
