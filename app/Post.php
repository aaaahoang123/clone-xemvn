<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function create_by() {
        return $this->belongsTo('App\Account');
    }

    public function category() {
        return $this->belongsTo('App\PostCategory', 'category_id');
    }
}
