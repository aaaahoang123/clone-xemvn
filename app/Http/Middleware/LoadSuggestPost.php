<?php

namespace App\Http\Middleware;

use App\Post;
use Closure;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;

class LoadSuggestPost
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $suggestHotVideo = Cache::store('file')->get('suggestHotVideo', function () {
            $result = Post::where(['status' => 1, 'type' => 1])->orderBy('seen_count', 'desc')->paginate(3);
            Cache::store('file')->put('suggestHotVideo', $result, 10);
            return $result;
        });
        $suggestNewImg = Cache::store('file')->get('suggestNewImg', function () {
            $result = Post::where(['status' => 1, 'type' => 2])->orderBy('id', 'desc')->paginate(3);
            Cache::store('file')->put('suggestNewImg', $result, 10);
            return $result;
        });
        $suggestRandom = Cache::store('file')->get('suggestRandom', function () {
            $result = Post::inRandomOrder()->paginate(3);
            Cache::store('file')->put('suggestRandom', $result, 10);
            return $result;
        });

        View::share([
            'suggestHotVideo' => $suggestHotVideo,
            'suggestNewImg' => $suggestNewImg,
            'suggestRandom' => $suggestRandom
        ]);
        return $next($request);
    }
}
