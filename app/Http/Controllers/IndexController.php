<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class IndexController extends Controller
{
    private $expireCacheTime = 10;
    private $limitResultDefault = 5;

    /**
     * @param Request $req
     * @return data of image
     * @path '/'
     */
    public function index(Request $req)
    {
        $data = Cache::store('file')->get('index-'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 2)->orderBy('id', 'desc')->paginate($limit==null?10:$limit);
            Cache::store('file')->put('index-'.$req->getQueryString(), $result, 10);
            return $result;
        });
        return $data;
    }

    /**
     * - Get a single image, and return.
     * - May use by route as api controller, or use by a view controller as a query.
     * @param $handle_url
     * @return mixed
     */
    public function singleImage($handle_url)
    {
        $SingleImageQuery = Post::where('handle_url', $handle_url);
        $SingleImageQuery->increment('seen_count', 1);

        $SingleImage = Cache::store('file')->get('image-'.$handle_url, function () use ($handle_url, $SingleImageQuery) {
            $result = $SingleImageQuery->firstOrFail();
            Cache::store('file')->put('image-'.$handle_url, $result, $this->expireCacheTime);
            return $result;
        });

        return $SingleImage;
    }

    /**
     * - Find old images.
     * @param Request $req
     * @return mixed
     */
    public function old(Request $req) {
        $oldImages = Cache::store('file')->get('old?'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 2)->paginate($limit==null?$this->limitResultDefault:$limit);
            Cache::store('file')->put('old?'.$req->getQueryString(), $result, $this->expireCacheTime);
            return $result;
        });

        return $oldImages;
    }

    /**
     * - Find hot images.
     * - Can use by route as api controller. Or use by another view controller as a query function.
     * @param Request $req
     * @return mixed
     */
    public function hot(Request $req) {
        $hotImages = Cache::store('file')->get('hot?'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 2)->orderBy('seen_count', 'desc')->paginate($limit==null?$this->limitResultDefault:$limit);
            Cache::store('file')->put('hot?'.$req->getQueryString(), $result, $this->expireCacheTime);
            return $result;
        });

        return $hotImages;
    }

    /**
     * Use $this->>index() to query and return view.
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function indexView(Request $req)
    {
        $data = $this->index($req);
        return view('index.home')->with([
            "listContent" => $data
        ]);
    }

    /**
     * use $this->singleImage() to query and return view.
     * @param $handle_url
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function singleImageView($handle_url)
    {
        $SingleImage = $this->singleImage($handle_url);
        $thisId = $SingleImage->id;

        $backItem = Cache::store('file')->get('image-'.$handle_url.'-back', function () use ($handle_url, $thisId) {
            $result = Post::where('id', $thisId-1)->first();
            if ($result != null) Cache::store('file')->put('image-'.$handle_url.'-back', $result, $this->expireCacheTime);
            return $result;
        });

        $nextItem = Cache::store('file')->get('image-'.$handle_url.'-next', function () use ($handle_url, $thisId) {
            $result = Post::where('id', $thisId+1)->first();
            if ($result != null) Cache::store('file')->put('image-'.$handle_url.'-next', $result, $this->expireCacheTime);
            return $result;
        });

        $newItems = Cache::store('file')->get('newImages-page=1', function () {
           $result = Post::where('type', 2)->orderBy('id', 'DESC')->paginate(8);
           Cache::store('file')->put('newImages-page=1', $result, $this->expireCacheTime);
           return $result;
        });

        return view('index.image')->with(
            [
                "post" => $SingleImage,
                "newPost" => $newItems,
                "backUrl" => $backItem,
                "nextUrl" => $nextItem
            ]
        );
    }

    /**
     * Only return the new images view. That the js can bind to the view with ajax.
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadNewImageComponentView(Request $req) {
        $key = 'newImages-page='.$req->query('page')!=null?$req->query('page'):1;

        $newItems = Cache::store('file')->get($key, function () use ($key) {
            $result = Post::where('type', 2)->orderBy('id', 'DESC')->paginate(8);
            Cache::store('file')->put($key, $result, $this->expireCacheTime);
            return $result;
        });

        return view('index.new-image-component')->with(["newPost" => $newItems]);
    }

    /**
     * - use $this->old() to query, and return the view.
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function oldView(Request $req) {
        $oldImages = $this->old($req);
        return view('index.home')->with([
            "listContent" => $oldImages
        ]);
    }

    /**
     * - use $this -> hot() to query, and return the view.
     * @param Request $req
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function hotView(Request $req) {
        $hotImages = $this->hot($req);

        return view('index.home')->with([
            "listContent" => $hotImages
        ]);
    }
}
