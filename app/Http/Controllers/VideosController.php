<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class VideosController extends Controller
{
    private $expireCacheTime = 10;
    private $limitResultDefault = 5;

    public function index(Request $req)
    {
        $data = Cache::store('file')->get('videos?'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 1)->orderBy('id', 'desc')->paginate($limit==null?$this->limitResultDefault:$limit);
            Cache::store('file')->put('videos?'.$req->getQueryString(), $result, $this->expireCacheTime);
            return $result;
        });

        return $data;
    }

    public function singleVideo($handle_url)
    {
        $VideoItemsQuery = Post::where('handle_url', $handle_url);
        $VideoItemsQuery->increment('seen_count', 1);

        $VideoItem = Cache::store('file')->get('videos/'.$handle_url, function () use ($handle_url, $VideoItemsQuery) {
            $result = $VideoItemsQuery->firstOrFail();
            Cache::store('file')->put('videos/'.$handle_url, $result, $this->expireCacheTime);
            return $result;
        });

        return $VideoItem;
    }

    public function old(Request $req) {
        $oldVideos = Cache::store('file')->get('videos/old?'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 1)->orderBy('id')->paginate($limit==null?$this->limitResultDefault:$limit);
            Cache::store('file')->put('videos/old?'.$req->getQueryString(), $result, $this->expireCacheTime);
            return $result;
        });

        return $oldVideos;
    }

    public function hot(Request $req) {
        $hotVideos = Cache::store('file')->get('videos/hot?'.$req->getQueryString(), function () use ($req) {
            $limit = $req->query('limit');
            $result = Post::where('type', 1)->orderBy('seen_count', 'desc')->paginate($limit==null?$this->limitResultDefault:$limit);
            Cache::store('file')->put('videos/hot?'.$req->getQueryString(), $result, $this->expireCacheTime);
            return $result;
        });

        return $hotVideos;
    }

    public function indexView(Request $req)
    {
        $data = $this->index($req);
        return view('videos.index')->with([
            "listContent" => $data
        ]);
    }

    public function singleVideoView($handle_url)
    {
        $VideoItem = $this->singleVideo($handle_url);
        $thisId = $VideoItem->id;

        $backItem = Cache::store('file')->get('videos/'.$handle_url.'-back', function () use ($handle_url, $thisId) {
            $result = Post::where('id', $thisId-1)->first();
            if ($result != null) Cache::store('file')->put('videos/'.$handle_url.'-back', $result, $this->expireCacheTime);
            return $result;
        });

        $nextItem = Cache::store('file')->get('videos/'.$handle_url.'-next', function () use ($handle_url, $thisId) {
            $result = Post::where('id', $thisId+1)->first();
            if ($result != null) Cache::store('file')->put('videos/'.$handle_url.'-next', $result, $this->expireCacheTime);
            return $result;
        });

        $newItems = Cache::store('file')->get('newVideos?page=1', function () {
            $result = Post::where('type', 1)->orderBy('id', 'DESC')->paginate(8);
            Cache::store('file')->put('newVideos?page=1', $result, $this->expireCacheTime);
            return $result;
        });
        return view('videos.single-item')->with([
            "post" => $VideoItem,
            "newPost" => $newItems,
            "backUrl" => $backItem,
            "nextUrl" => $nextItem
        ]);
    }

    public function loadNewVideoComponentView(Request $req) {
        $key = 'newVideos?page='.$req->query('page')!=null?$req->query('page'):1;
        $newItems = Cache::store('file')->get($key, function () use ($key) {
            $result = Post::where('type', 1)->orderBy('id', 'DESC')->paginate(8);
            Cache::store('file')->put($key, $result, $this->expireCacheTime);
            return $result;
        });
        return view('videos.new-video-component')->with(["newPost" => $newItems]);
    }

    public function oldView(Request $req) {
        $oldVideos = $this->old($req);
        return view('videos.index')->with([
            "listContent" => $oldVideos
        ]);
    }

    public function hotView(Request $req) {
        $hotVideos = $this->hot($req);

        return view('videos.index')->with([
            "listContent" => $hotVideos
        ]);
    }
}
