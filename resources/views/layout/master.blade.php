<!doctype html>
<html lang="en">
<head>

    @include('layout.head')
    <style>
        a:hover {
            text-decoration: none!important;
        }
        a.hover-violet:hover {
            color: #cb1c6b!important;
        }
    </style>
</head>
<body>


<!-- TOP HEADER Start
    ================================================== -->
@if(Route::is('videos.*') || Route::is('videos'))
    @include('layout.videos-header')
@else
    @include('layout.header')
@endif



@yield('content')




<!-- CALL TO ACTION Start
================================================== -->

@include('layout.footer')

{{--@include('includes.script')--}}

@yield('script')

</body>
</html>
