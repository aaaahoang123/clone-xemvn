<style>
    .custom-crop-article {
        display: inline-block;
        width: 100%;
        vertical-align:center;
        horiz-align: center;
    }
    .custom-crop-thumb {
        height: 150px;
        width: 100%;
        background: #3e3e3e no-repeat center center;
        background-size: cover;
    }
</style>
@foreach($newPost as $ni)
    <div class="col-md-3 col-sm-4 mt-2 custom-crop-article">
        <a href="/{{$ni -> handle_url}}" class="thumbnail-url" title="{{$ni->title}}">
            <div class="custom-crop-thumb" style="background-image: url('{{$ni->thumbnail}}')"></div>
            {{--<img src="{{$ni -> thumbnail}}" class="w-100" alt="{{$ni->title}}">--}}
            <p class="limit-text">{{$ni -> title}}</p>
            <span>
                <i class="fas fa-eye"></i>
                {{$ni -> seen_count}}
            </span>
        </a>
    </div>
@endforeach
